import axios from "axios";
import qs from "qs";
import i18n from "@/i18n/index";
import { message } from "ant-design-vue";
import { baseURLMap } from "./path";
import { codeMsg } from "./errMsg";
import router from "@/router";

const apiUrl = baseURLMap["product"];

axios.defaults.baseURL = apiUrl;
axios.defaults.timeout = 90000;
let lang = localStorage.getItem("lang");
if (!lang) {
  lang = navigator.language;
  if (navigator.language == "zh-CN" || navigator.language == "zh-TW") {
    lang = "zh";
  }
}

axios.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem("token");
    if (token) {
      config.headers.Authorization = token;
    }
    config.headers.language = lang === "zh" ? "0" : "1";
    return config;
  },
  (err) => {
    Promise.reject(err);
  }
);

// Pre-process response
axios.interceptors.response.use(
  (response) => {
    if (
      response.config.responseType === "blob" ||
      response.config.responseType === "arraybuffer"
    ) {
      console.log("blob");
      return response;
    } else if (response.data && response.data.code !== 200) {
      const code = response.data.code;
      switch (code) {
        case 700003:
          message.error(i18n.global.t("verificationCodeError"));
          break;
        default:
          message.error(response.data.msg);
          break;
      }
      return false;
    } else {
      if (response.data.data === 0) {
        return response.data.data;
      }
      return response.data.data || true;
    }
  },
  (err) => {
    if (err.response && err.response.status === 401) {
      message.error(codeMsg[lang].code401);
      router.push("/login");
      localStorage.setItem("token", null);
      return false;
    }
    console.log(err);
    return false;
  }
);

const http = {
  postDocImgStream(url, param, responseType = "blob") {
    return new Promise((resolve) => {
      axios({
        method: "post",
        responseType,
        url,
        data: param,
      }).then((res) => {
        resolve(res);
      });
    });
  },
  post(url, param) {
    return new Promise((resolve) => {
      axios({
        method: "post",
        url,
        data: param,
      }).then((res) => {
        resolve(res);
      });
    });
  },
  postForm(url, param) {
    return new Promise((resolve) => {
      axios({
        method: "post",
        url,
        data: qs.stringify({ ...param }),
      }).then((res) => {
        resolve(res);
      });
    });
  },
  base: axios,
};

http.install = (Vue) => {
  Vue.prototype.$http = http;
};

export default http;
