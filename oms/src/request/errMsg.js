export const codeMsg = {
  zh: {
    code401: "登录信息已失效，请重新登录",
  },
  en: {
    code401: "The login information is invalid. Please log in again",
  },
};
