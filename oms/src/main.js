import { createApp, watch } from "vue";
import { createPinia } from "pinia";
import i18n from "./i18n/index";

import Antd from "ant-design-vue";
import App from "./App.vue";
import router from "./router";
import "ant-design-vue/dist/antd.css";

const app = createApp(App);
const pinia = createPinia();

if (localStorage.getItem("state")) {
  pinia.state.value = JSON.parse(localStorage.getItem("state"));
}

watch(
  pinia.state,
  (state) => {
    localStorage.setItem("state", JSON.stringify(state));
  },
  { deep: true }
);

app.use(Antd);
app.use(pinia);
app.use(router);
app.use(i18n);

app.mount("#app");
