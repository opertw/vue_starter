import { createRouter, createWebHistory } from "vue-router";
import { useUserStore } from "@/stores/user";

const routes = [
  {
    path: "/",
    name: "Home",
    component: () => import("@/views/home/HomeView.vue"),
    meta: {
      requiresAuth: true,
    },
    redirect: "/dashboard",
    children: [
      {
        path: "/transaction",
        name: "transaction",
        component: () => import("@/views/transaction/FormalTransaction.vue"),
        meta: {
          requiresAuth: true,
        },
      },
      {
        path: "/dashboard",
        name: "dashboard",
        component: () => import("@/views/home/components/SummaryComp.vue"),
        meta: {
          requiresAuth: true,
        },
      },
      {
        path: "/adjusted",
        name: "adjusted",
        component: () => import("@/views/home/adjusted/SummaryComp.vue"),
        meta: {
          requiresAuth: true,
        },
      },
    ],
  },
  {
    path: "/login",
    name: "login",
    component: () => import("@/views/login/LoginView.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
});

router.beforeEach((to) => {
  if (to.meta.requiresAuth && !useUserStore().isAuth) {
    return { name: "login", query: { redirect: to.fullPath } };
  }
});

export default router;
