import request from "@/request/http";
export function login(params) {
  return request.postForm(`/login`, params);
}

export function getVerificationCode(params) {
  return request.postDocImgStream(`/createCode`, params);
}

export function checkVerificationCode(params) {
  return request.post(`/checkCode`, params);
}
