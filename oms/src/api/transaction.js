import request from "@/request/http";

export function formalTransList(params) {
  return request.post(`/formalTrans/list`, params);
}
