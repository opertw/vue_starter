import request from "@/request/http";

export function queryTradeTodoCount(params) {
  return request.post(`/queryTradeTodoCount`, params);
}

export function countPending(params) {
  return request.post(`/preAuthTrade/countPending`, params);
}

export function queryReportList(params) {
  return request.post(`/homepage/queryReportList`, params);
}
