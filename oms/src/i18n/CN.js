import { transaction } from "./transaction/transaction_cn";
import { navigation } from "./navigation/navigation_cn";
import { home } from "./home/home_cn";
import { login } from "./login/login_cn";
export default {
  login,
  transaction,
  navigation,
  home,
};
