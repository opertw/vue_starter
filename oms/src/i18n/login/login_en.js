export const login = {
  signIn: "User Login",
  welcome: "Welcome Back!",
  title: "Make Global Payment Easy",
  username: "Username",
  password: "Password",
  submit: "Submit",
};
