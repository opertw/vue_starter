export const login = {
  signIn: "账号登录",
  welcome: "欢迎回来!",
  title: "让全球支付更简单",
  username: "用户名",
  password: "密码",
  submit: "登录",
};
