import { home } from "./home/home_en";
import { navigation } from "./navigation/navigation_en";
import { transaction } from "./transaction/transaction_en";
import { login } from "./login/login_en";
export default {
  login,
  home,
  transaction,
  navigation,
};
