import zh from "./CN.json";
import en from "./EN.json";
import { createI18n } from "vue-i18n";

let lang = navigator.language;
if (navigator.language == "zh-CN" || navigator.language == "zh-TW") {
  lang = "zh";
}

const i18n = createI18n({
  legacy: false,
  locale: lang,
  messages: {
    zh: zh,
    en: en,
  },
});

export default i18n;
